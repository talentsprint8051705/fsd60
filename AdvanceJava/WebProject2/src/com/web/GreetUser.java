package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/GreetUser")
public class GreetUser extends HttpServlet {
	

   
    public GreetUser() {
  
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("username");
	    response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    out.println("<html><body bgcolor='skyblue'>");
	    out.println("<h1>Greetings " + username + "</h1>");
	    out.println("</body></html>");
	    
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		    doGet(request, response);
	}

}
