package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dto.Employee;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		HttpSession session = request.getSession(false);
		Employee employee = (Employee) session.getAttribute("employee");
			
		request.getRequestDispatcher("EmpHomePage.jsp").include(request, response);
			
		out.print("<br/><table border='2' align='center'>");

		out.print("<tr>");
		out.print("<th> EmpId    </th>");
		out.print("<th> EmpName  </th>");
		out.print("<th> Salary   </th>");
		out.print("<th> Gender   </th>");
		out.print("<th> Email-Id </th>");
		out.print("<th> Password </th>");
		out.print("</tr>");

		out.print("<tr>");
		out.print("<td>" + employee.getEmpId()    + "</td>");
		out.print("<td>" + employee.getEmpName()  + "</td>");
		out.print("<td>" + employee.getSalary()   + "</td>");
		out.print("<td>" + employee.getGender()   + "</td>");
		out.print("<td>" + employee.getEmailId()  + "</td>");
		out.print("<td>" + employee.getPassword()  + "</td>");
		out.print("</tr>");	

		out.print("</table>");
	}

 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}