package assignments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.db.DbConnection;
import java.util.*;

public class FetchAssign {

	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Employee ID to fetch:");
		int empId = scanner.nextInt();

		String fetchQuery = "SELECT * FROM employee WHERE empId = ?";

		try {
			preparedStatement = connection.prepareStatement(fetchQuery);
			preparedStatement.setInt(1, empId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				System.out.println("Employee Found:");
				System.out.println("Employee ID: " + resultSet.getInt("empId"));
				System.out.println("Employee Name: " + resultSet.getString("empName"));
				System.out.println("Salary: " + resultSet.getDouble("salary"));
				System.out.println("Gender: " + resultSet.getString("gender"));
				System.out.println("Email ID: " + resultSet.getString("emailId"));
				System.out.println("Password: " + resultSet.getString("password"));
			} else {
				System.out.println("No employee found with ID " + empId);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		scanner.close();
	}
}
