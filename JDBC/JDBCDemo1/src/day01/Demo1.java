package day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Demo1 {

	public static void main(String[] args) {
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/fsd60";
		
		try {
		
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			//2. Establishing the Connection
			con = DriverManager.getConnection(url, "root", "root");
			
			if (con != null) {
				System.out.println("Successfully Established the Connection");
				
				con.close();
				
			} else {
				System.out.println("Failed to Establish the Connection");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}

