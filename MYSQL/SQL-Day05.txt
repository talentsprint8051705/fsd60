SQL Day-05
----------

1. Having Clause
2. Joins (Inner Join, Left Outer Join, Right Outer Join, Cross Joins, Self Join)
3. Sub-Queries


Having Clause
-------------

select gender, sum(salary) from employee group by gender having gender='Male';
elect gender, sum(salary) from employee group by gender having gender='Female'; 


groupby, having, orderby
------------------------

select gender, count(salary) from employee group by gender having count(salary) > 1;
select gender, count(salary) from employee group by gender having count(salary) > 1 order by count(salary);
select gender, count(salary) from employee group by gender having count(salary) > 1 order by count(salary) asc;
select gender, count(salary) from employee group by gender having count(salary) > 1 order by count(salary) desc;


EmpId, EmpName, Salary, Gender, Doj, EmailId, Password, Designation, ManagerId;
101    Harsha   1212.12 Male    --   ----     ----      Trainer        104
102    Pasha    2222.22 Male    ---  ----     ----      Trainer        104
104    Indira   2121.12 Female  --   ----     ----      Manager        108
108    Ashokan  3434.34 Male    --   ----     ----      DepartmentHead 111


***********************************************************************


Joins
-----
1. Inner Join
2. Left Join
3. Right Join
4. Cross Join

Course
------
Create table course (cid int, cname varchar(20), fees double);
insert into course values (1, 'C', 3000), (2, 'C++', 4000), (3, 'Java', 5000), (4, 'MySQL', 6000), (5, 'Python', 7000);
select * from course;
+------+--------+------+
| cid  | cname  | fees |
+------+--------+------+
|    1 | C      | 3000 |
|    2 | C++    | 4000 |
|    3 | Java   | 5000 |
|    4 | MySQL  | 6000 |
|    5 | Python | 7000 |
+------+--------+------+

Student
-------
Create table student (sid int, sname varchar(20), cid int);
insert into student values (101, 'Harsha', 2), (102, 'Pasha', 3), (103, 'Vamsi', 4), (104, 'Utkarsh', 3), (105, 'Krishna', 6);
select * from student;

+------+---------+------+
| sid  | sname   | cid  |
+------+---------+------+
|  101 | Harsha  |    2 |
|  102 | Pasha   |    3 |
|  103 | Vamsi   |    4 |
|  104 | Utkarsh |    3 |
|  105 | Krishna |    6 |
+------+---------+------+



Alias for Tables
-----------------
select * from student;
select sid, sname, cid from student;
select s.sid, s.sname, s.cid from student s;


1. Inner Join
-------------
select s.sid, s.sname, c.cid, c.cname, c.fees from student s inner join course c where s.cid = c.cid;


2. Left Join
-------------
select s.sid, s.sname, c.cid, c.cname, c.fees from student s left join course c on s.cid = c.cid;
select s.sid, s.sname, c.cid, c.cname, c.fees from course c left join student s on s.cid = c.cid;


3. Right Join
-------------
select s.sid, s.sname, c.cid, c.cname, c.fees from student s right join course c on s.cid = c.cid;
select s.sid, s.sname, c.cid, c.cname, c.fees from course c right join student s on s.cid = c.cid;


4. Cross Join
-------------
select s.sid, s.sname, c.cid, c.cname, c.fees from student s, course c;
select s.sid, s.sname, c.cid, c.cname, c.fees from student s inner join course c;




***********************************************************************


Sub-Queries
-----------

Using the Same Tables Student and Course
----------------------------------------

select * from student where cid = 2;
select * from course where cname='C++';
select cid from course where cname='C++';
select cid from course where cname='C++';


select * from student where cid = (select cid from course where cname='C++');
select * from student where cid = (select cid from course where cname='Java');
select * from student where cid = (select cid from course where cname='C');
select * from student where cid = (select cid from course where cname='MySQL');
select * from student where cid = (select cid from course where cname='Python');

select * from course where fees > 6000;		//1 record
select * from course where fees > 1000;		//5 or all records

select cid from course where fees > 6000;	//1 record
select cid from course where fees > 1000;	//5 or all records

select * from student where cid = (select cid from course where fees > 6000);
select * from student where cid = (select cid from course where fees > 1000);
ERROR 1242 (21000): Subquery returns more than 1 row

select * from student where cid >= (select cid from course where fees > 6000);
select * from student where cid >= (select cid from course where fees > 1000);
ERROR 1242 (21000): Subquery returns more than 1 row

In, Not In
----------
select * from student where cid in (select cid from course where fees > 1000);
select * from student where cid not in (select cid from course where fees > 1000);



Operators
---------
select * from course;
select cid from course where fees = 5000;
select * from student where cid = (select cid from course where fees = 5000);
select * from student where cid >= (select cid from course where fees = 5000);
select * from student where cid <= (select cid from course where fees = 5000);

Any, All
--------
select * from student where cid =any (select cid from course where fees >= 5000);
select * from student where cid >any (select cid from course where fees >= 5000);
select * from student where cid <any (select cid from course where fees >= 5000);
select * from student where cid =any (select cid from course where fees >= 5000);
select * from student where cid =all (select cid from course where fees >= 5000);
select * from student where cid >all (select cid from course where fees >= 5000);
select * from student where cid >any (select cid from course where fees >= 5000);




***********************************************************************