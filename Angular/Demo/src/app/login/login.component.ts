import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  emailId: any;
  password: any;
  employees: any;
  emp: any;

  //Implementing the Dependency Injection for Router Class and EmpService
  constructor(private router: Router, private service: EmpService) { 

    this.employees = [
      {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   doj:'12-13-2018', country:'IND', emailId:'harsha@gmail.com', password:'123'},
      {empId:102, empName:'Pasha',  salary:2323.23, gender:'Male',   doj:'11-14-2019', country:'SWD', emailId:'pasha@gmail.com',  password:'123'},
      {empId:103, empName:'Indira', salary:3434.34, gender:'Female', doj:'10-15-2020', country:'CHI', emailId:'indira@gmail.com', password:'123'},
      {empId:104, empName:'Venkat', salary:4545.45, gender:'Male',   doj:'09-16-2021', country:'NEP', emailId:'venkat@gmail.com', password:'123'},
      {empId:105, empName:'Gopi',   salary:5656.56, gender:'Male',   doj:'08-17-2022', country:'IND', emailId:'gopi@gmail.com',   password:'123'},
      {empId:106, empName:'Pavan',  salary:6767.67, gender:'Male',   doj:'07-18-2023', country:'IND', emailId:'pavan@gmail.com',  password:'123'}
    ];
  }

  loginSubmit(loginForm: any) {
    //Storing the emailId value under localstorage
    localStorage.setItem('emailId', loginForm.emailId);
        
    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {
      
      //Setting the LoginStatus to True under Emp.Service
      this.service.isUserLoggedIn();
      this.router.navigate(['showemps']);

    } else {
      
      this.emp = null;

      this.employees.forEach((employee: any) =>  {
        if (loginForm.emailId == employee.emailId && loginForm.password == employee.password) {
          this.emp = employee;
        }
      });

      if (this.emp != null) {

        //Setting the LoginStatus to True under Emp.Service
        this.service.isUserLoggedIn();
        this.router.navigate(['products']);

      } else {
        alert('Invalid Credentials');
        this.router.navigate(['']);
      }
    }
  }

}



